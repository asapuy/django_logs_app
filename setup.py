from setuptools import setup, find_packages
from codecs import open

from os import path

here = path.abspath(path.dirname(__file__))


with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='django_logs_app',
    version='1.0.0',
    description='Django app that logs data',
    long_description=long_description,
    author='asap developers',
    author_email='info@asapdevelopers.com',
    keywords='django log logger python',
    packages=find_packages(),
    install_requires=['django']
)