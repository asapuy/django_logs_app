﻿from django.db import models


class ErrorLog(models.Model):
    level = models.CharField(max_length=50)
    log_name = models.CharField(max_length=255, blank=True, null=True)
    file_name = models.CharField(max_length=1024, blank=True, null=True)
    line_number = models.IntegerField(blank=True, null=True)

    user_id = models.IntegerField(blank=True, null=True)
    date = models.DateTimeField()

    message = models.CharField(max_length=10240, blank=True, null=True)
    extra = models.CharField(max_length=10240, blank=True, null=True)

    def __unicode__(self):
        return str(self.pk)

    class Meta(object):
        verbose_name = "Error Log"
        verbose_name_plural = "Error Logs"
