from django import forms
from django.contrib import admin

from django_logs_app import models


class ErrorLogAdmin(admin.ModelAdmin):
    model = models.ErrorLog
    list_display = ('date', 'level', 'log_name', 'message')

    list_filter = ['level', 'log_name', 'file_name']

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super(ErrorLogAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name in ('message', 'extra'):
            formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
        return formfield


admin.site.register(models.ErrorLog, ErrorLogAdmin)
