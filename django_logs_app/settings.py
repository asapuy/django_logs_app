import os
POOL_SIZE_FACTOR = 1
SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY", 'asap0k)3p4l!9)5x_0^==5%io8qx*l=%(yu2(nhscjb04f1+%61v^5asap')
RESET_TOKEN_SECRET_KEY = os.environ.get("RESET_TOKEN_SECRET_KEY", 'asap0k)3p4l!9)5x_0^==5%io8qx*l=%(yu2(nhscjb04f1+%61v^5asap')

INSTALLED_APPS = [
    'django_logs_app'
]