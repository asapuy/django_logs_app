DJANGO LOGGER APP
=================

This package implements a Django logger to save errors and show them on Django Admin

Provides
--------
- Console logging.
- Thread pooled DB logging
- ModelAdmin for DB logs.


Provides
--------
- Console logging.
- Thread pooled DB logging
- ModelAdmin for DB logs.

Dependencies
------------
- None

Installation
------------
1. Add 'logs_app' to installed apps
2. Configure loggers, for example:

        LOGGING['handlers'] = {
            'console': {
                'level': 'DEBUG',
                'class': 'django_logs_app.loggers.ConsoleLogger',
                'formatter': 'verbose',
            },
            'db': {
                'level': 'DEBUG',
                'class': 'django_logs_app.loggers.CentralErrorLogger',
                'formatter': 'onlyMessage',

            }
        }

3. Enjoy
